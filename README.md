PAIPline: Pathogen identification in metagenomic and clinical next generation sequencing samples
======================================================================================

Get the tool
------------

Please visit https://gitlab.com/andreas.andrusch/paipline to find the
latest source code and documentation.